//funcional
function sejaBemVindo(){
    console.log("Seja bem vindo a formação javascript");
}

console.log("Chamando a função Seja bem vindo sejaBemVindo(): ");
sejaBemVindo();

//POO
var objTeste = {

    nome: 'Cleison França',
    curso: 'Formação JavaScript',
    mistrarAula: function(){
        console.log("Ministrando aula de javascript");
    }
};
console.log("Objeto teste");
console.log(objTeste);

console.log("acessando propriedades do objeto: objTeste.nome ");
console.log(objTeste.nome);
console.log(objTeste.curso);

console.log("chamando o metodod do objeto: objTeste.ministrarAula()");
objTeste.mistrarAula();