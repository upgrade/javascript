//declaração de variáveis
var nome = "Cleison";
var sobreNome = "França";

console.log("Instrução solta em um arquivo js");

//declaração de função
function exibirNomeConsole(){
    console.log("exibir nome: " + nome );
}

function exibirNomeCompleto(){
    console.log("Nome Completo: " + nome + " " + sobreNome);
}

function eventoClick(){
    alert("Você clicou em um botao");
}



//chamadas de função
exibirNomeConsole();
