//literal
var itens  = {};
var itens2 = {nome: "refrigerante", 
                preco: 13, 
                ativo: true,
                detalhes: {
                    detalhes: '..'
                }};
//objeto pelo Construtor
var pessoa = new Object();
pessoa.nome = "joão";
pessoa.idade = 44;

//acessando
console.log(itens2.nome);
console.log(itens2.preco);
console.log(itens2['ativo']);
console.log(pessoa.nome);
console.log(pessoa.idade);
console.log(itens2.detalhes.detalhes);