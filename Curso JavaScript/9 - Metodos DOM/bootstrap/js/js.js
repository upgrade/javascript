var nomeBootinputText = window.document.getElementById("nomeBoot");
var estadoSelect = document.getElementById("estadoSelectBoot");

function selecionarCampos() {
  console.log("typeof: " + typeof nomeBootinputText);
  console.log(
    "object call: " + Object.prototype.toString.call(nomeBootinputText)
  );
  //alert(nomeBootinputText)
  console.log("Recuperar valor com value: " + nomeBootinputText.value);
  //nomeBootinputText.disabled = true;
  //console.log("Disabled: nomeBootinputText.disabled = true");
  nomeBootinputText.readOnly = true;
  console.log("Readonly: nomeBootinputText.readOnly = true;");

  console.log("tagname: " +nomeBootinputText.tagName);
  console.log("tagname type: " +nomeBootinputText.type);

}
function selecionarCamposSelect() {
    console.log(
        "object call: " + Object.prototype.toString.call(estadoSelect)
      );
      console.log("estadoSelect.value" + estadoSelect.value);
      console.log("estadoSelect.selectedIndex" + estadoSelect.selectedIndex);
}

var emailPromocionalCheck = document.querySelector("#emailPromocionalCheckBoot");

function selecionarCampoEmailCheck(){
    console.log("Object call tipo: " + Object.prototype.toString.call(emailPromocionalCheck));
    console.log("Tag name: " + emailPromocionalCheck.tagName);
    console.log("Tag name type: " + emailPromocionalCheck.type);
    console.log("Valor: " + emailPromocionalCheck.value);
    console.log("checked: " + emailPromocionalCheck.checked);
}

var formaConbtatoRadio = document.querySelector("[name=customRadio]");

function selecionarCampoRadio(){
    console.log("Object call tipo: " + Object.prototype.toString.call(formaConbtatoRadio));
    console.log("Tag name: " + formaConbtatoRadio.tagName);
    console.log("Tag name type: " + formaConbtatoRadio.type);
    console.log("Valor: " + formaConbtatoRadio.value);
    console.log("checked: " + formaConbtatoRadio.checked);
}

var radios = document.getElementsByName("customRadio");

function selecionarCamposRadios(){
    console.log("Object call tipo: " + Object.prototype.toString.call(radios));
    
}

var formulario = document.querySelector("#formBoot");

console.log("form tipo: " + Object.prototype.toString.call(formulario));

function exibirDados(elemento){
    console.log(elemento);
}