var idade  = 28;
console.log(idade);
console.log(typeof idade);

var precoFeijao = 2.89;
console.log(precoFeijao);
console.log(typeof precoFeijao);

var valor = 23 + 33;
console.log(valor);

var valor = +"23" + 33;
var numero = -38;
console.log(valor);
console.log(numero);
console.log(numero + valor);

console.log(valor.toString(10)); //decimal
console.log(valor.toString(2)); //binario