console.log("linha 1");
//throw new Error("Ocorreu um erro na aplicação");
console.log("linha 2");

try{
    console.log(soma(10, new Array(10)));
}catch(erro){
    //console.log(erro);
    console.log(erro.name);
    console.log(erro.message);
    console.log(erro.stack);
}finally{
    console.log("Sempre sera executado");
}

function soma(a, b){
    //return a + b;
    //return a / b;
    //return a[3];
    return a.exec();
}