var data  = new Date();
console.log(typeof data);
console.log(data);

var dataString = new Date('2017-10-10');
console.log(dataString);

console.log(dataString.getFullYear());
console.log(dataString.getMonth() + 1);
console.log(dataString.getDay());
console.log(dataString.getDate());

var dataParam = new Date(2018, 04, 17);
console.log(dataParam);