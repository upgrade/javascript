//declaracao de variaveis
var nome = "Estudante Cleison";
var sobreNome = "França";

console.log('Instrução solta em um arquivo JS');

function exibirNomeConsole(){
    console.log('Exibir nome: ' + nome);
}

function exibirNomeCompleto(){
    console.log("Nome Completo: " + nome + ' ' + sobreNome);
}

function eventoClick(){
    alert('você clicou em um botão');
}


//chamada de funcoes
exibirNomeConsole();
