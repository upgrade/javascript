//string
//toString

var nome = new String("Maria da Silva");
console.log("1 tipo", typeof nome);
console.log("2 tipo", typeof nome.toString());


//length
console.log("length: ", nome.length);

//iterar na string
for(var indice in nome){
    console.log("indice: " + indice, nome[indice]);
    
}

//recuperar valor a partir do index
console.log(nome.charAt(9));

//recuperando o codigo utf16 pelo indice
console.log(nome.charCodeAt(9));

//recuperando o indice pelo valor 
console.log(nome.indexOf("S"));

//recuperando o indice do ultimo caracterer pesquisado
console.log(nome.lastIndexOf("a"));

//retorna um codigo html
console.log("sub", nome.sub());
console.log("sup", nome.sup());

//buscar uma quantidade de caracter a partir de um indice
console.log(nome.substr(0, 5));
console.log(nome.substr(0, nome.length));

//buscar parte de uma string informando o indice inicial e final
console.log(nome.substr(0, 10));
console.log(nome.substr(5, nome.length));
console.log(nome.substr(5));

//replace - substituir dados em uma string
console.log(nome.replace("a", "?"));
console.log(nome.replace(/a/g, "?"));

nome = nome.replace(/\?/g, function(x){
    return "a";
})
console.log(nome);

//split dividir a string
console.log(nome.split());
console.log(nome.split(""));
console.log(nome.split(" "));
console.log(nome.split(" ", 1));
console.log(nome.split(/ da /g));

//texto em caixa alta e baixa
console.log(nome.toLowerCase());
console.log(nome.toUpperCase());

//match - realizar uma pesquisa com regexp
var texto = "Casa de Papel de Madeira de Ferro";
var resultado = texto.match(/de/g);
console.log(Object.prototype.toString.call(resultado));
console.log(resultado);


//search
var resultado2 = texto.search(/de/g);
console.log(Object.prototype.toString.call(resultado2));
console.log(resultado2);
