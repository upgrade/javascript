/*var nome = "Cleison";
var sobrenome = 'França';
var nomeCompleto = nome + " "+ sobrenome;
var texto = "";

console.log("Nome Completo: " + nomeCompleto);

var idade = 28;
console.log(idade);

var precoFeijao = 2.89;
console.log(precoFeijao);
console.log(typeof precoFeijao);
*/

//valores literais
var nomeCurso = "Formação javaScript";
var nomeCurso2 = '2 - formação javascript';
var nomeCurso3 = new String('3 - formação javascript');

console.log(nomeCurso);
console.log(nomeCurso2);
console.log(nomeCurso3.toString());
