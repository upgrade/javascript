//expressao regular
var regexpLiteral = /JS/; //literal
var regexpConstrutor = new RegExp("J"); //construtor
var stringCurso = "formação completa javascript - Mestre JS Jedi";

console.log(regexpLiteral.test(stringCurso));
console.log(regexpConstrutor.exec(stringCurso));
