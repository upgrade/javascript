/*
//literal
var itens = {};
var itens2 = {nome: "refrigerante", preco: 23, ativo:true, detalhe: {detalhe: '...'}};

//construtor
var pessoa  = new Object();
pessoa.nome = "joão";
pessoa.idade = 44;

//acessar
console.log(itens2.nome);
console.log(itens2.preco);
console.log(itens2['ativo']);
console.log(pessoa.nome);
console.log(pessoa.idade);
console.log(itens2.detalhe.detalhe);

//array literal
var produtos = [];
var produtos2 = ['Produto A', 'Produto B', 'Produto C'];
var valores = ['Pedro', "maria", 45, true, {nome: 'Antonio'}];

//array pelo construtor
var array = new Array();
var array2 = new Array('A', 23, false);

//acessar array
console.log(array2[0]);
console.log(array2[2]);
console.log(array2[1]);
console.log(valores[4].nome);

*/

function exibirNoConsole(mensagem){
    console.log(mensagem);
}

exibirNoConsole("teste");

function soma(v1, v2){
    return v1 + v2;
}
console.log(soma(20, 10));







