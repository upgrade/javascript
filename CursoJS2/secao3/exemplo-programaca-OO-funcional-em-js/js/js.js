
//programação funcional
function sejaBemVindo(){
    console.log('Seja bem vindo a formação JavaScript Mestre Jedi');
    
}

console.log("Chamando a função seja Bem vindo sejaBemVindo(): " );
sejaBemVindo();

//POO
var objProfessor = {
    nome : 'Estudante Cleison França',
    curso : 'Formação JavaScript Mestre Jedi',
    mistrarAula: function(){
        console.log("Ministrando aula de JavaScript!");
    }
}

console.log('Obj professor');
console.log(objProfessor);

console.log('Acessando propriedade do objeto: objProfessor.nome');
console.log(objProfessor.nome);
console.log(objProfessor.curso);

console.log('Chamando o metodo do objeto professor: objProfessor.mistrarAula()');
objProfessor.mistrarAula();

